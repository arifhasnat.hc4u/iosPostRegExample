//
//  ViewController.swift
//  AlamofirePostRequest
//
//  Created by Arif on 3/28/17.
//  Copyright © 2017 Arif. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    @IBOutlet var loginStatusLabel: UILabel!
    @IBOutlet var email: UITextField!
    
    @IBOutlet var password: UITextField!
    
    
    @IBOutlet var regStatus: UILabel!
    

    @IBOutlet var nameReg: UITextField!
    @IBOutlet var emailReg: UITextField!
    @IBOutlet var passReg: UITextField!
    @IBOutlet var phoneReg: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
    
        
        
       }
 

    @IBAction func loginBtn(_ sender: Any) {
        
        login()
        
        
    }
    
    
    @IBAction func regBtn(_ sender: Any) {
        registration()
    }
    
    
    func login() {
     
              let parameters: Parameters = [
            "securityCode": "api#ruetAPI#34J@H1RS009TA12#SACXY879JJSEC09879COD12E",
            "email": email.text!,
            "password": password.text!
            
        ]
        
        
        Alamofire.request("http://appsplorer-bd.com/ruetapi/login", method: .post, parameters: parameters).responseJSON { response in
            
            
            let allData = JSON(response.result.value!)
            let sucessStatus = allData["success"]
            
            if sucessStatus == true {
                
                print("done")
                self.loginStatusLabel.text = "Login Success "
                
                
            }else{
                self.loginStatusLabel.text = "Login Failed "
            }
            
            
        }

    }
    
    
    func registration() {
        
        let parameters: Parameters = [
            "securityCode": "api#ruetAPI#34J@H1RS009TA12#SACXY879JJSEC09879COD12E",
            "name": nameReg.text!,
            "email": emailReg.text!,
            "password": passReg.text!,
            "phone": phoneReg.text!,
            "student_id": "default",
            "facebook_id": "default",
            "blood_group": "default",
            "admission_date": "default",
            "job_description": "default",
            "lat": "default",
            "long": "default",
            "location": "default",
            "present_address": "default",
            "permanent_address": "default",
            "profile_pic": "default"
           
            

            
        ]
        
        
        Alamofire.request("http://appsplorer-bd.com/ruetapi/register", method: .post, parameters: parameters).responseJSON { response in
            
            
            let allData = JSON(response.result.value!)
            let sucessStatus = allData["success"]
            
            
            if sucessStatus == true {
                
                print("done")
                self.regStatus.text = "Registraion Success "
                
                
            }else{
                self.regStatus.text = "Registration Failed "
            }

            
            
        }
        
    }
    

   

   

}

